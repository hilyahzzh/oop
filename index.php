<?php
    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    $sheep = new Animal("shaun");
    echo "Name : " .$sheep->name . "<br>"; 
    echo "Legs : " .$sheep->legs . "<br>"; 
    echo "Cold Blooded : " .$sheep->cold_blooded . "<br> <br>"; 

    $kodok = new Frog("buduk");
    echo "Name : " .$sheep->name . "<br>"; 
    echo "Legs : " .$sheep->legs . "<br>"; 
    echo "Cold Blooded : " .$sheep->cold_blooded . "<br>"; 
    echo $kodok->jump() . "<br><br>"; // "hop hop"
    

    $sungokong = new Ape("kera sakti");
    echo "Name : " .$sheep->name . "<br>"; 
    echo "Legs : " .$sheep->legs . "<br>"; 
    echo "Cold Blooded : " .$sheep->cold_blooded . "<br>"; 
    $sungokong->yell() ; // "Auooo"
?>